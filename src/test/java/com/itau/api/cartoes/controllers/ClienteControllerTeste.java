package com.itau.api.cartoes.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.api.cartoes.auth.JWTUtil;
import com.itau.api.cartoes.DTOs.CartaoDTO;
import com.itau.api.cartoes.models.*;
import com.itau.api.cartoes.services.CartaoService;
import com.itau.api.cartoes.services.ClienteService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

@WebMvcTest(ClienteController.class)
@Import(JWTUtil.class)
public class ClienteControllerTeste {

    @MockBean
    private ClienteService clienteService;

    @MockBean
    private CartaoService cartaoService;

    @Autowired
    private MockMvc mockMvc;

    Cliente cliente;
    List<Cartao> cartoes;

    @BeforeEach
    private void setUp(){
        this.cliente= new Cliente();
        cliente.setId(1);
        cliente.setNome("Ingrid Monalisa Bicudo");
        cliente.setCpf("39025176895");
        cliente.setSenha("123445");
    }

    @Test
    @WithMockUser(username = "ingrid.bicudo@usp.br", password = "aviao11")
    public void testarCadastrarCliente() throws Exception {
        Mockito.when(clienteService.cadastrarCliente(Mockito.any(Cliente.class))).thenReturn(cliente);

        ObjectMapper objectMapper = new ObjectMapper();
        String clienteJson = objectMapper.writeValueAsString(cliente);

        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                .contentType(MediaType.APPLICATION_JSON).content(clienteJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Ingrid Monalisa Bicudo")));


    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartao() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(10000);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra("15");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComVarianteErrada() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("TESTE");
        cartaoDTO.setLimite(10000);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra("15");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComBandeiraErrada() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(10000);
        cartaoDTO.setBandeira("BLA");
        cartaoDTO.setDataDaMelhorCompra("15");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComLimiteInferiorA1000() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(1);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra("15");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComDataDaMelhorCompraVazia() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(1);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra("");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComDataDaMelhorCompraNula() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(1);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra(null);

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComLimiteErrado() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(10000.12354);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra("15");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComBandeiraVazia() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(10000);
        cartaoDTO.setBandeira("");
        cartaoDTO.setDataDaMelhorCompra("15");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComBandeiraNula() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(10000);
        cartaoDTO.setBandeira(null);
        cartaoDTO.setDataDaMelhorCompra("15");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComVarianteVazia() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("");
        cartaoDTO.setLimite(10000);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra("15");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComVarianteNula() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante(null);
        cartaoDTO.setLimite(10000);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra("15");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComDataDaMelhorCompraMenorQue0() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(10000);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra("0");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser()
    public void testarSolicitarCartaoComDataDaMelhorCompraMaiorQue28() throws Exception
    {
        //Preparação
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setVariante("BLACK");
        cartaoDTO.setLimite(10000);
        cartaoDTO.setBandeira("MASTER");
        cartaoDTO.setDataDaMelhorCompra("50");

        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(clienteService.solicitarCartao(Mockito.any(CartaoDTO.class), Mockito.anyInt())).thenReturn(cartao);

        ObjectMapper objectMapper = new ObjectMapper();
        String cartaoDTOJson = objectMapper.writeValueAsString(cartaoDTO);

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/clientes/1/solicitarcartao")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoDTOJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
