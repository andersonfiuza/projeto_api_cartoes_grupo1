package com.itau.api.cartoes.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.api.cartoes.DTOs.CartaoDTO;
import com.itau.api.cartoes.DTOs.CartaoDtValidDTO;
import com.itau.api.cartoes.auth.JWTUtil;
import com.itau.api.cartoes.models.Bandeira;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Status;
import com.itau.api.cartoes.models.Variante;
import com.itau.api.cartoes.services.CartaoService;
import com.itau.api.cartoes.services.ClienteService;
import com.itau.api.cartoes.services.CompraService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(CartaoController.class)
@Import(JWTUtil.class)
public class CartaoControllerTeste {

    @MockBean
    private CartaoService cartaoService;

    @MockBean
    private ClienteService clienteServiceService;

    @MockBean
    private CompraService compraServiceService;

    @Autowired
    private MockMvc mockMvc;

    Cartao cartao;

    @BeforeEach
    public void setUp (){
        this.cartao = new Cartao();
        cartao.setVariante(Variante.PLATINUM);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(5000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(142);
        cartao.setNumero("1231.1231.1231.1231");

    }

    @Test
    @WithMockUser(username = "ingrid.bicudo@usp.br", password = "aviao11")
    public void testarSalvarCartao () throws Exception {
        Mockito.when(cartaoService.salvar(Mockito.any(Cartao.class))).thenReturn(cartao);
        ObjectMapper objectMapper = new ObjectMapper();

        String cartaoJson = objectMapper.writeValueAsString(cartao);

        mockMvc.perform(MockMvcRequestBuilders.post("/cartoes")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated());
                //.andExpect(MockMvcResultMatchers.jsonPath("$.numero", CoreMatchers.equalTo("1231.1231.1231.1231")));
    }

    @Test
    @WithMockUser(username = "ingrid.bicudo@usp.br", password = "aviao11")
    public void testarAlterarLimite () throws Exception {
        Mockito.when(cartaoService.salvar(Mockito.any(Cartao.class))).thenReturn(cartao);
        ObjectMapper objectMapper = new ObjectMapper();

        String cartaoJson = objectMapper.writeValueAsString(cartao);

        mockMvc.perform(MockMvcRequestBuilders.put("/cartoes/1/atualizalimite")
                .contentType(MediaType.APPLICATION_JSON).content(cartaoJson))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
