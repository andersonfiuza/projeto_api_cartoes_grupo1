package com.itau.api.cartoes.services;

import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.repositories.ClienteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ClienteServiceTeste {

    @MockBean
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    Cliente cliente;
    List<Cartao> cartoes;

    @BeforeEach
    private void setUp (){
        this.cliente= new Cliente();
        cliente.setId(1);
        cliente.setNome("Ingrid Monalisa Bicudo");
        cliente.setCpf("39025176895");
        cliente.setSenha("123445");
    }

    @Test
    public void testarBuscaDeClientePeloId (){
        Optional<Cliente>  clienteOptional = Optional.of(cliente);
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(clienteOptional);

        Cliente clienteTeste = clienteService.buscarClientePeloId(12);

        Assertions.assertEquals(cliente, clienteTeste);
        Assertions.assertEquals(cliente.getNome(), clienteTeste.getNome());
    }

    @Test
    public void testarBuscaDeClientePeloIdQueNaoExiste (){
        Optional<Cliente>  clienteOptional = Optional.empty();
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(clienteOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {clienteService.buscarClientePeloId(42);});
    }


    @Test
    public void testarSalvarCliente (){
        Cliente cliente = new Cliente();
        cliente.setNome("Maria Clara");
        cliente.setCpf("61281217050");
        cliente.setSenha("54321");

        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).then(objeto -> objeto.getArgument(0));

        Cliente testeDeCliente = clienteService.cadastrarCliente(cliente);

        Assertions.assertEquals("Maria Clara", testeDeCliente.getNome());
    }


}
