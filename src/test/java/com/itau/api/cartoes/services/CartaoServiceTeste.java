package com.itau.api.cartoes.services;

import com.itau.api.cartoes.DTOs.CartaoDTO;
import com.itau.api.cartoes.models.Bandeira;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Status;
import com.itau.api.cartoes.models.Variante;
import com.itau.api.cartoes.repositories.CartaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class CartaoServiceTeste {

    @MockBean
    private CartaoRepository cartaoRepository;

    @Autowired
    private CartaoService cartaoService;

    Cartao cartao;

    @BeforeEach
    public void setUp (){
        this.cartao = new Cartao();
        cartao.setVariante(Variante.PLATINUM);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(5000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(142);
        cartao.setNumero("1231.1231.1231.1231");

    }


    @Test
    public void testarSalvar()
    {
        //Preparação
        Cartao cartao = new Cartao();
        cartao.setVariante(Variante.BLACK);
        cartao.setStatus(Status.Ativo);
        cartao.setLimite(10000);
        cartao.setDataValidade("2002");
        cartao.setBandeira(Bandeira.MASTER);
        cartao.setDataDaMelhorCompra("15");
        cartao.setCvv(123);
        cartao.setNumero("1234.1234.1234.1234");

        Mockito.when(cartaoRepository.save(Mockito.any(Cartao.class))).thenReturn(cartao);

        //Execução
        Cartao cartaoCriado = cartaoService.salvar(cartao);

        //Verificação
        Assertions.assertEquals(cartao, cartaoCriado);
    }

    @Test
    public void testarCriar()
    {
        //Preparação
        CartaoDTO cartao = new CartaoDTO();
        cartao.setVariante("BLACK");
        cartao.setLimite(10000);
        cartao.setBandeira("MASTER");
        cartao.setDataDaMelhorCompra("15");

        //Execução
        Cartao cartaoCriado = cartaoService.criar(cartao);

        //Verificação
        Assertions.assertNotNull(cartaoCriado);
        Assertions.assertEquals(Variante.BLACK, cartaoCriado.getVariante());
        Assertions.assertEquals(Bandeira.MASTER, cartaoCriado.getBandeira());
    }

    @Test
    public void testarBuscarPeloIdDoCartao (){
        Optional<Cartao> optionalCartao = Optional.of(cartao);

        Mockito.when(cartaoRepository.findById(Mockito.anyInt())).thenReturn(optionalCartao);

        Cartao cartaoTeste = cartaoService.buscarCartaoPeloId(1212);

        Assertions.assertEquals(cartao, cartaoTeste);
        Assertions.assertEquals(cartao.getNumero(), cartaoTeste.getNumero());

    }

    @Test
    public void testarBusCartaoPeloNumero (){
        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(cartao);

        Cartao cartaoTeste = cartaoService.buscarCartao(cartao.getNumero());

        Assertions.assertEquals(cartaoTeste.getBandeira(), cartao.getBandeira());
    }

    @Test
    public void testarAlterarLimite (){
        Mockito.when(cartaoRepository.save(Mockito.any(Cartao.class))).then(objeto -> objeto.getArgument(0));

        Cartao cartaoTeste = cartaoService.salvar(cartao);

        Assertions.assertEquals(cartaoTeste.getLimite(), cartao.getLimite());
    }

    @Test
    public void testarAlterarDataValidade (){
        Mockito.when(cartaoRepository.save(Mockito.any(Cartao.class))).then(objeto -> objeto.getArgument(0));

        Cartao cartaoTeste = cartaoService.salvar(cartao);

        Assertions.assertEquals(cartaoTeste.getDataValidade(), cartao.getDataValidade());
    }

}
