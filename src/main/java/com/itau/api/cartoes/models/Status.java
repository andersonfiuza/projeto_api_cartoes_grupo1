package com.itau.api.cartoes.models;

public enum Status
{
    Cancelado,
    Ativo
}
