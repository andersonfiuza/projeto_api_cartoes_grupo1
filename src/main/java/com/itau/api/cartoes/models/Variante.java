package com.itau.api.cartoes.models;

public enum Variante {
    BLACK("BLACK"),
    GOLD("GOLD"),
    PLATINUM("PLATINUM");

    Variante(String nome) {
        this.nome = nome;
    }

    private String nome;

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return nome;
    }
}
