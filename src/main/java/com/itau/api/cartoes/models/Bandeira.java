package com.itau.api.cartoes.models;

public enum Bandeira {
    MASTER ("MASTER"),
    VISA ("VISA");

    Bandeira(String Nome){
        this.nome = Nome;
    }

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return nome;
    }
}
