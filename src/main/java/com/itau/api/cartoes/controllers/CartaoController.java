package com.itau.api.cartoes.controllers;

import com.itau.api.cartoes.DTOs.CompraDTO;
import com.itau.api.cartoes.DTOs.CancelaCrtDTO;
import com.itau.api.cartoes.DTOs.CartaoDtValidDTO;
import com.itau.api.cartoes.DTOs.CartaoLimiteDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.models.Compra;
import com.itau.api.cartoes.services.CartaoService;
import com.itau.api.cartoes.services.CompraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartoes")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @Autowired
    CompraService compraService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao salvar(@RequestBody @Valid Cartao cartao) {
        return cartaoService.salvar(cartao);
    }


    @PostMapping("/registrarcompra")
    @ResponseStatus(HttpStatus.CREATED)
    public Compra registrarcompra(@RequestBody @Valid CompraDTO compraDTO) throws Exception {
        try {

            Compra objCompra = compraService.registrarcompra(compraDTO);

            return objCompra;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @PutMapping("/{id}/atualizalimite")
    public Cartao atualizarLimiteCartao(@RequestBody @Valid CartaoLimiteDTO selecionaCartaoLimite, @PathVariable(name = "id") int id) {
        try {
            Cartao cartaoDB = cartaoService.atualizarLimiteCartao(id, selecionaCartaoLimite.getLimite());
            return cartaoDB;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}/atualizadatadamelhorcompra")
    public Cartao atualizarDataDaMelhorCompra(@RequestBody @Valid CartaoDtValidDTO selecionaCartaoDtValid, @PathVariable(name = "id") int id) {
        try {
            Cartao dtValidade = cartaoService.atualizarDataDaMelhorCompra(id, selecionaCartaoDtValid.getDtValidadeCrt());
            return dtValidade;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}/cancelacartao")
    public Cartao cancelarCartao(@RequestBody @Valid CancelaCrtDTO selecionacrtstatus, @PathVariable(name = "id") int id) {
        try {
            Cartao statuscrt = cartaoService.cancelarCartao(id, selecionacrtstatus.getStatusCrt());
            return statuscrt;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@PathVariable(name = "id") int id) {
        try {
            cartaoService.deletarCartao(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Cartao pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Cartao cartao = cartaoService.buscarCartaoPeloId(id);
            return cartao;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

