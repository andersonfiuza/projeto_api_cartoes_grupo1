package com.itau.api.cartoes.services;


import com.itau.api.cartoes.DTOs.CompraDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.models.Compra;
import com.itau.api.cartoes.models.Status;
import com.itau.api.cartoes.repositories.ClienteRepository;
import com.itau.api.cartoes.repositories.CompraRepository;
import jdk.nashorn.api.scripting.ScriptUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class CompraService {




    @Autowired
    private CompraRepository compraRepository;

    @Autowired
    private CartaoService cartaoService;


    public Compra registrarcompra(CompraDTO compraDTO) throws Exception {

        Cartao cartao = cartaoService.buscarCartao(compraDTO.getNumero());

        if(cartao == null)
        {
            throw new RuntimeException("Número do cartão informado é inválido");
        }

        Date dataEnviada = new SimpleDateFormat("yyyy-MM-dd").parse(compraDTO.getDataValidade());
        Date dataDoCartao = new SimpleDateFormat("yyyy-MM-dd").parse(cartao.getDataValidade());


        if (!compraDTO.getNumero().equals(cartao.getNumero())) {

            throw new RuntimeException("Número do cartão informado é inválido");
        } else if (compraDTO.getCvv() != cartao.getCvv()) {

            throw new RuntimeException("Cvv do cartão informado é inválido");

        } else if (cartao.getStatus() == Status.Cancelado) {
            throw new RuntimeException("Cartão Cancelado, favor informar um cartão valido para compra!");

        }else if(dataEnviada.before(dataDoCartao) ) {
            throw new RuntimeException("Data de validade do cartão expirada");
        }

        Compra compra = compraDTO.converterParaCompra();
        compra.setCartao(cartao);

        return compraRepository.save(compra);


    }


}
