package com.itau.api.cartoes.services;


import com.itau.api.cartoes.auth.AuthCliente;
import com.itau.api.cartoes.DTOs.CartaoDTO;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService implements UserDetailsService {


    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Cliente cadastrarCliente (Cliente cliente){
        String senha = cliente.getSenha();

        cliente.setSenha(encoder.encode(senha));
        return  clienteRepository.save(cliente);
    }

    public Cliente buscarClientePeloId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return  cliente;
        }else {
            throw new RuntimeException("O cliente não foi encontrado");
        }
    }

    public Cartao solicitarCartao(CartaoDTO cartao, Integer id)
    {
        Cliente cliente = buscarClientePeloId(id);

        if(cliente != null)
        {
            Cartao novoCartao = cartaoService.criar(cartao);
            novoCartao.setCliente(cliente);
            novoCartao = cartaoService.salvar(novoCartao);
            return novoCartao;
        }
        else
        {
            throw new RuntimeException("Cliente não encontrado");
        }
    }

    // Valida usuário através do CPF
    @Override
    public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
        Cliente usuario = clienteRepository.findByCpf(cpf);
        if(usuario == null ){
            throw new UsernameNotFoundException("CPF não cadastrado");
        }
        AuthCliente authCliente = new AuthCliente(usuario.getId(), usuario.getCpf(), usuario.getSenha());
        return authCliente;
    }
}
