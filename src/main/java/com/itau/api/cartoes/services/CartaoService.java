package com.itau.api.cartoes.services;


import com.itau.api.cartoes.DTOs.CartaoDTO;
import com.itau.api.cartoes.models.Bandeira;
import com.itau.api.cartoes.models.Cartao;
import com.itau.api.cartoes.models.Status;
import com.itau.api.cartoes.models.Variante;
import com.itau.api.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Optional;

import java.time.LocalDate;
import java.util.Random;


@Service
public class CartaoService {

//**** Cria/Salva o Cartao
    @Autowired
    private CartaoRepository cartaoRepository;

    public Cartao salvar(Cartao cartao) {

        return cartaoRepository.save(cartao);
    }
    //**** Busca o Cartao pelo ID
    public Cartao buscarCartaoPeloId(int id){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }else {
            throw new RuntimeException("O numero do cartao não foi encontrado");
        }
    }
    //**** Atualiza o limite do cartão
    public Cartao atualizarLimiteCartao(int id, double limite){
        Cartao cartaoDB = buscarCartaoPeloId(id);

        if(limite==0){
            throw new RuntimeException("Limite não pode ser zero");
        }
        else {
            cartaoDB.setLimite(limite);
            return cartaoRepository.save(cartaoDB);
        }
    }
    //**** Atualiza a Data de Validade
    public Cartao atualizarDataDaMelhorCompra(int id, String dtvalidade){
        Cartao dtValidade = buscarCartaoPeloId(id);

        dtValidade.setDataDaMelhorCompra(dtvalidade);
        return cartaoRepository.save(dtValidade);
    }
    //**** Cancela o cartão - mudando o status para C - cancelado
    public Cartao cancelarCartao(int id, Boolean status) {
        Cartao statuscrt = buscarCartaoPeloId(id);

        if(!status)
            statuscrt.setStatus(Status.Cancelado);
        else
            statuscrt.setStatus(Status.Ativo);
        return cartaoRepository.save(statuscrt);
    }

    //**** Deleta o cartão
    public void deletarCartao(int id){
        if(cartaoRepository.existsById(id)) {
            cartaoRepository.deleteById(id);
        }else {
            throw new RuntimeException("Cartão não existe");
        }
    }


    public Cartao buscarCartao(String numero) {

        return  cartaoRepository.findByNumero(numero);
    }

    public Cartao criar(CartaoDTO cartaoDTO)
    {
        Cartao cartao = new Cartao();

        for (Bandeira bandeira : Bandeira.values())
        {
            if(bandeira.toString().toUpperCase().equals(cartaoDTO.getBandeira().toUpperCase()))
                cartao.setBandeira(bandeira);
        }

        Random random = new Random();

        cartao.setCvv(random.nextInt(999));
        cartao.setDataDaMelhorCompra(cartaoDTO.getDataDaMelhorCompra());

        cartao.setDataValidade(LocalDate.now().plusYears(4).toString());
        cartao.setLimite(cartaoDTO.getLimite());

        cartao.setNumero( String.format ("%04d", random.nextInt(9999)) + "." +
                String.format ("%04d", random.nextInt(9999)) + "." +
                String.format ("%04d", random.nextInt(9999)) + "." +
                String.format ("%04d", random.nextInt(9999)));

        cartao.setStatus(Status.Ativo);

        for (Variante variante : Variante.values())
        {
            if(variante.toString().toUpperCase().equals(cartaoDTO.getVariante().toUpperCase()))
                cartao.setVariante(variante);
        }

        return cartao;
    }

}
