package com.itau.api.cartoes.auth;

import com.itau.api.cartoes.services.ClienteService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroDeAutorizacao extends BasicAuthenticationFilter {

    private JWTUtil jwtUtil;
    private ClienteService clienteService;

    public FiltroDeAutorizacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil,
                               ClienteService usuarioService) {
        super(authenticationManager);
        this.jwtUtil = jwtUtil;
        this.clienteService = usuarioService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        String authorizationHeader = request.getHeader("Authorization");

        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            //validações
            String tokenLimpo = authorizationHeader.substring(7);
            UsernamePasswordAuthenticationToken authToken = getAutenticao(request, tokenLimpo);

            if(authToken != null){
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAutenticao(HttpServletRequest request, String token){
        if(jwtUtil.tokenValido(token)){
            String cpf = jwtUtil.getCpf(token);
            UserDetails usuario = clienteService.loadUserByUsername(cpf);
            return new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
        }else{
            return null;
        }
    }
}
