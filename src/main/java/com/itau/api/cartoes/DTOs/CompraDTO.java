package com.itau.api.cartoes.DTOs;

import com.itau.api.cartoes.models.Compra;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CompraDTO {


    private String numero;

    @DecimalMin(value = "0", message = "Limite deve ser maior ou igual a zero")
    private int cvv;


    @NotNull(message = "Data da validade do cartão não pode ser nulo ")
    @NotBlank(message ="Data da validade não pode ser em branco.")
    private String dataValidade;

    @DecimalMin(value = "0", message = "Valor da compra deve ser maior ou igual a zero")
    @Digits(integer = 6, fraction = 2, message = "Valor da compra fora do padrão")
    private double valordacompra;

    @NotNull(message = "Data da compra não pode ser nulo ")
    @NotBlank(message ="Data da compra não pode ser em branco.")
    private String datadacompra;

    @NotNull(message = "Local da compra não pode ser nulo ")
    @NotBlank(message ="Local da compra não pode ser em branco.")
    private String localdacompra;


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public String getDataValidade() {
        return dataValidade;
    }

    public void setDataValidade(String dataValidade) {
        this.dataValidade = dataValidade;
    }



    public double getValordacompra() {
        return valordacompra;
    }

    public void setValordacompra(double valordacompra) {
        this.valordacompra = valordacompra;
    }

    public String getDatadacompra() {
        return datadacompra;
    }

    public void setDatadacompra(String datadacompra) {
        this.datadacompra = datadacompra;
    }

    public String getLocaldacompra() {
        return localdacompra;
    }

    public void setLocaldacompra(String localdacompra) {
        this.localdacompra = localdacompra;
    }



    public Compra converterParaCompra(){
        Compra compra = new Compra();
        compra.setLocaldacompra(this.localdacompra);
        compra.setDatadacompra(this.datadacompra);
        compra.setValordacompra(this.valordacompra);
        return compra;
    }



}
