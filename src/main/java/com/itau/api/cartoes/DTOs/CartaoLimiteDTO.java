package com.itau.api.cartoes.DTOs;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

public class CartaoLimiteDTO {
// captura somnente o limite do Cartao
private double limite;

        @NotNull(message =  "Limite não pode ser nulo")
        @DecimalMin(value = "1000", message = "Limite deve ser maior ou igual a 1000")
        public Double getLimite() {
            return limite;
        }

        public void setLimite(Double limite) {
            this.limite = limite;
        }

}


