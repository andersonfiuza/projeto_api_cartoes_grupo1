package com.itau.api.cartoes.DTOs;

import javax.validation.constraints.*;

public class CartaoDTO
{
    @NotNull(message =  "Data da melhor compra não pode ser nula")
    @NotBlank(message = "Data da melhor compra não pode ser vazia")
    @DecimalMin(value = "1", message = "Data da melhor compra deve ser maior ou igual a 1")
    @DecimalMax(value = "28", message = "Data da melhor compra deve ser menor ou igual a 28")
    private String dataDaMelhorCompra;
    @NotNull(message =  "Limite não  pode ser nulo")
    @DecimalMin(value = "1000", message = "Limite deve ser maior ou igual a 1000")
    @Digits(integer = 6, fraction = 2, message = "Limite fora do padrão")
    private double limite;
    @NotNull(message =  "Bandeira não pode ser nula")
    @NotBlank(message = "Bandeira não pode ser vazia")
    private String bandeira;
    @NotNull(message =  "Variante não  pode ser nula")
    @NotBlank(message = "Variante não pode ser vazia")
    private String variante;

    public CartaoDTO() {
    }

    public String getDataDaMelhorCompra() {
        return dataDaMelhorCompra;
    }

    public void setDataDaMelhorCompra(String dataDaMelhorCompra) {
        this.dataDaMelhorCompra = dataDaMelhorCompra;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public String getBandeira() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    public String getVariante() {
        return variante;
    }

    public void setVariante(String variante) {
        this.variante = variante;
    }
}
