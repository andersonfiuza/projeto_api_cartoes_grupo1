package com.itau.api.cartoes.DTOs;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CartaoDtValidDTO {

    // captura somnente da Data de Validade do Cartao
    @NotNull(message =  "Data da melhor compra não pode ser nula")
    @NotBlank(message = "Data da melhor compra não pode ser vazia")
    @DecimalMin(value = "1", message = "Data da melhor compra deve ser maior ou igual a 1")
    @DecimalMax(value = "28", message = "Data da melhor compra deve ser menor ou igual a 28")
    private String dtvalidadecrt;

    public String getDtValidadeCrt() {
        return dtvalidadecrt;
    }

    public void setDtvalidadecrt(String dtvalidadecrt) {
        this.dtvalidadecrt = dtvalidadecrt;
    }
}