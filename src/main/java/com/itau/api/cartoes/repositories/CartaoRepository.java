package com.itau.api.cartoes.repositories;
import com.itau.api.cartoes.models.Compra;
import org.springframework.data.jpa.repository.JpaRepository;
import com.itau.api.cartoes.models.Cartao;
import java.util.List;

import com.itau.api.cartoes.models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartaoRepository  extends JpaRepository<Cartao,Integer > {

    Cartao findByNumero(String numero);

}
