package com.itau.api.cartoes.repositories;

import com.itau.api.cartoes.DTOs.CompraDTO;
import com.itau.api.cartoes.models.Cliente;
import com.itau.api.cartoes.models.Compra;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompraRepository  extends JpaRepository<Compra,Integer > {
}
